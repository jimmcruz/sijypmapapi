// Import express
let express = require('express')
// Initialize the app
let app = express();
// Import Body parser
let bodyParser = require('body-parser');
// Import Mongoose
let mongoose = require('mongoose');

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
   extended: true
}));
app.use(bodyParser.json());
// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/sijypgeo', function(error) {
  if (error) throw error;
  console.log('DB Successfully connected');
});
var db = mongoose.connection;

// Setup server port
var port = process.env.PORT || 5051;

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));
// Launch app to listen to specified port

// Import routes
let apiRoutes = require("./api-routes")
// Use Api routes in the App
app.use('/apisgeo', apiRoutes)

app.listen(port, function () {
     console.log("Running RestHub on port " + port);
});
