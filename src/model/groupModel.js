// groupModel.js
var mongoose = require('mongoose')
// Setup schema
var groupSchema = mongoose.Schema({
    idgroup: {
        type: Number,
        required: true
    },
    group: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Group model
var Group = module.exports = mongoose.model('group', groupSchema);
module.exports.get = function (callback, limit) {
    Group.find(callback).limit(limit);
}
