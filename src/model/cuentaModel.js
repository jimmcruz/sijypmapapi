// cuentaModel.js
var mongoose = require('mongoose')
// Setup schema
var cuentaSchema = mongoose.Schema({
    idgrupo: {
        type: Number,
        required: true
    },
    idrow: {
        type: Number,
        required: true
    },
    conteo: {
        type: Number,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Cuenta model
var Cuenta = module.exports = mongoose.model('cuenta', cuentaSchema);
module.exports.get = function (callback, limit) {
    Cuenta.find(callback).limit(limit);
}
