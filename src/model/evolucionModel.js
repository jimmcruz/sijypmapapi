// evolucionModel.js
var mongoose = require('mongoose')
// Setup schema
var evolucionSchema = mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    idrow: {
        type: Number,
        required: true
    },
    anno: {
        type: Number,
        required: true
    },
    mes: {
        type: String,
        required: true
    },
    municipios: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Evolucion model
var Evolucion = module.exports = mongoose.model('evolucion', evolucionSchema);
module.exports.get = function (callback, limit) {
    Evolucion.find(callback).limit(limit);
}
