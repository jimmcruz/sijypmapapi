// Import Cuenta model
Cuenta = require('../model/cuentaModel');
// Handle index actions
exports.index = function (req, res) {
    Cuenta.find( {idgrupo: req.params.id},
      {idrow: 1, conteo: 1, _id: 0},
      function (err, cuentas) {
        if (err) {
            res.json({
                status: 'error',
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Cuentas retrieved successfully",
            data: cuentas
        })
    }).sort({grupo: 1})
}
