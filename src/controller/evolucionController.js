// Import Evolucion model
Evolucion = require('../model/evolucionModel');

// Handle view evolucion info
exports.index = function (req, res) {
    Evolucion.find( {id: req.params.id},
        {idrow:1, anno:1, mes:1, municipios:1, ids:1, _id:0},
        function (err, evoluciones) {
      if (err) {
          res.json({
              status: 'error',
              message: err,
          });
      }
      /*
      for(var e in evoluciones){
        evoluciones[e].ids = evoluciones[e].municipios.split(',').map(item => {
            return parseInt(item, 10);
        })
        console.log(evoluciones[e])
      }*/
      res.json({
          status: "success",
          message: "Evolucion retrieved successfully for " + req.params.id,
          data: evoluciones
      })
    }).sort({idrow: 1})
}
