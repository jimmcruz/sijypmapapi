// Import Grupo model
Group = require('../model/groupModel');
// Handle index actions
exports.index = function (req, res) {
    Group.find( {},
      {idgrupo: 1, grupo: 1, _id: 0 },
      function (err, groups) {
        if (err) {
            res.json({
                status: 'error',
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Grupos retrieved successfully",
            data: groups
        })
    }).sort({grupo: 1})
}
