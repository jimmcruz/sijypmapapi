// Initialize express router
let router = require('express').Router()
// Set default API response
router.get('/', function (req, res) {
    res.json({
       status: 'API Its Working',
       message: 'Welcome to RESTHub crafted with love!'
    })
})

// Import controller
var evolucionController = require('./src/controller/evolucionController')
var groupController = require('./src/controller/groupController')
var cuentaController = require('./src/controller/cuentaController')
// routes
router.route('/grupo_armado/:id')
    .get(evolucionController.index)
router.route('/grupo_armado/')
    .get(groupController.index)
router.route('/grupo_armado_h/:id')
    .get(cuentaController.index)

// Export API routes
module.exports = router;
